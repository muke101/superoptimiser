#![feature(core_ffi_c)]
use std::ffi::*;
use core::ffi::*;
use llvm_sys::prelude::*;
use llvm_sys::core::*;
use std::io::prelude::*;
use std::iter::Cycle;

#[derive(Eq, PartialEq, Copy, Clone)]
enum LLVMType	{
	I64,
	I32,
	I16,
	I8,
	I1,
	F64,
	F32
}

enum Instruction	{
	UnaOp(UnaryInstruction),
	BinOp(BinaryInstruction),
}

struct BinaryInstruction	{
	builder: unsafe extern "C" fn(LLVMBuilderRef, LLVMValueRef, LLVMValueRef, *const c_char) -> LLVMValueRef,
	operand_types: (LLVMType, LLVMType),
	return_type: LLVMType,
	associative: bool
}

struct UnaryInstruction	{
	builder: unsafe extern "C" fn(LLVMBuilderRef, LLVMValueRef, *const c_char) -> LLVMValueRef,
	operand_type: LLVMType,
	return_type: LLVMType
}

static OPS: &[Instruction] = &[
	Instruction::BinOp(BinaryInstruction { builder: LLVMBuildAdd, operand_types: (LLVMType::I64, LLVMType::I64), return_type: LLVMType::I64, associative: true}),
	Instruction::BinOp(BinaryInstruction { builder: LLVMBuildSub, operand_types: (LLVMType::I64, LLVMType::I64), return_type: LLVMType::I64, associative: false}),
	Instruction::UnaOp(UnaryInstruction { builder: LLVMBuildNeg, operand_type: LLVMType::I64, return_type: LLVMType::I64 }),
];

//TODO: probably fastest to delete the whole function and recreate it every time
fn super_optimise(input_values: &Vec<(LLVMValueRef, LLVMType)>, builder: LLVMBuilderRef, func_type: LLVMTypeRef,
				  module: LLVMModuleRef, input_len: usize)	{
	let mut value_stack = vec![];
	value_stack.push(OPS.iter());
	let mut sp = 0;
	while value_stack.len() < input_len	{
		while sp > 0	{
			//TODO: when sure this works can replace indexing with .get_unchecked for performance
			let op = value_stack[sp].next();
			match op	{
				Some(Instruction::UnaOp(op)) => {
					//if operands matched:
					//build IR - replace sp'th instruction (or ofc extend)
						if sp == value_stack.len() - 1	{
							//execute
						}
						else {
							sp += 1;
						}
					let mut operands: Vec<LLVMValueRef> = vec![];
					for val in value_pool.iter()	{
						if val.1 == op.operand_type {
							operands.push(val.0);
						}
					}
				}
				Some(Instruction::BinOp(op)) => {
					let mut operands: Vec<LLVMValueRef> = vec![];
					for val in value_pool.iter()	{
						if val.1 == op.operand_types.0 {
							operands.push(val.0);
						}
					}
				}
				None => {
					value_stack[sp] = OPS.iter();
					sp -= 1;
				}
			}
		}
		value_stack.push(OPS.iter());
		sp = 0;
	}
}

fn main()	{
	let mod_name = CString::new("output_module").unwrap();
	let output_module = unsafe { LLVMModuleCreateWithName(mod_name.as_ptr()) };
	//parse input module from cmdline with C wrapping, copy the input function type into the new module
	println!("Hello, world!");
}
